use std::sync::Arc;

use commons::storage::Database;

pub mod asset;

pub struct AssetRespository {
    db: Arc<Database>,
}

impl AssetRespository {
    pub fn new(db: Arc<Database>) -> Self {
        Self { db }
    }
}
