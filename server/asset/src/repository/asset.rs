use sqlx::{query, query_as};

use super::AssetRespository;
use crate::model::asset::Asset;

impl AssetRespository {
    pub(crate) async fn insert_asset(&self, asset: &Asset) -> Result<i32, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        let asset_id = query!(
            r#"INSERT INTO assets(name, description, owner, category_id) VALUES($1, $2, $3, $4) RETURNING ID"#,
            asset.name, asset.description, asset.owner, asset.category
        )
        .fetch_one(&mut connection)
        .await?;
        Ok(asset_id.id)
    }

    pub(crate) async fn asset_by_id(&self, id: i32) -> Result<Option<Asset>, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query_as!(
            Asset,
            r#"SELECT id, name, description, owner, category_id as category FROM assets WHERE id = $1"#,
            id
        )
        .fetch_optional(&mut connection)
        .await
    }

    pub(crate) async fn owned_assets(&self, owner_id: i32) -> Result<Vec<Asset>, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query_as!(
            Asset,
            r#"SELECT id, name, description, owner, category_id as category FROM assets WHERE owner = $1"#,
            owner_id
        )
        .fetch_all(&mut connection)
        .await
    }

    /// Returns all assets the user with given id has any permission to
    pub(crate) async fn accessable_assets(&self, user_id: i32) -> Result<Vec<Asset>, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query_as!(
            Asset,
            r#"
                SELECT a.id, a.name, a.description, a.owner, a.category_id as category
                FROM assets a
                LEFT JOIN grants g
                ON a.id = g.asset_id
                WHERE a.owner = $1 OR g.user_id = $1
            "#,
            user_id
        )
        .fetch_all(&mut connection)
        .await
    }

    /// tracks usage of an asset
    // TODO: optional timestamp?
    pub(crate) async fn use_asset(&self, user_id: i32, asset_id: i32) -> Result<(), sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query!(r#"INSERT INTO usages(user_id, asset_id) values($1, $2)"#, user_id, asset_id).execute(&mut connection).await?;
        Ok(())
    }
}
