// CREATE TABLE assets (
//     id INTEGER GENERATED ALWAYS AS IDENTITY,
//     name VARCHAR(100) NOT NULL,
//     description TEXT NOT NULL DEFAULT '',
//     owner INTEGER NOT NULL,
//     category_id INTEGER NOT NULL,
//     PRIMARY KEY(id),
//     CONSTRAINT fk_assets_owner FOREIGN KEY(owner) REFERENCES users(id),
//     CONSTRAINT fk_assets_category FOREIGN KEY(category_id) REFERENCES categories(id)
// );

use serde::{Deserialize, Serialize};

use crate::{repository::AssetRespository, AssetError};

#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Asset {
    #[serde(default)]
    pub id: i32,
    pub name: String,
    pub description: String,
    pub owner: i32,
    pub category: i32,
}

pub async fn create_asset(mut asset: Asset, repo: AssetRespository) -> Result<Asset, AssetError> {
    asset.id = repo
        .insert_asset(&asset)
        .await
        .map_err(AssetError::from_sqlx)?;
    Ok(asset)
}

pub async fn get_asset_by_id(asset_id: i32, repo: AssetRespository) -> Result<Asset, AssetError> {
    repo.asset_by_id(asset_id)
        .await
        .map_err(AssetError::from_sqlx)?
        .ok_or_else(|| AssetError::AssetNotFound(asset_id.to_string()))
}

pub async fn get_assets_by_owner(owner_id: i32, repo: AssetRespository) -> Result<Vec<Asset>, AssetError> {
    repo.owned_assets(owner_id).await.map_err(AssetError::from_sqlx)
}

/// returns all assets the user has any permission on
pub async fn get_assets_by_user(user_id: i32, repo: AssetRespository) -> Result<Vec<Asset>, AssetError> {
    repo.accessable_assets(user_id).await.map_err(AssetError::from_sqlx)
}

pub async fn use_asset(user_id: i32, asset_id: i32, repo: AssetRespository) -> Result<(), AssetError> {
    //FIXME: Add check for usage permission
    repo.use_asset(user_id, asset_id).await.map_err(AssetError::from_sqlx)
}