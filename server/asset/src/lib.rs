use actix_web::{ResponseError, http::{StatusCode, header::{self, HeaderValue}}, HttpResponse, web::BytesMut};
use std::fmt::Write;

pub mod model;
pub mod repository;

#[derive(thiserror::Error, Debug)]
pub enum AssetError {
    #[error("No matching asset found: {0}")]
    AssetNotFound(String),
    #[error("An unexpected server error occured: {0}")]
    InternalSystemError(String),
    #[error("No valid authorization received.")]
    UnauthorizedError,
}

impl AssetError {
    pub fn from_sqlx(db_error: sqlx::Error) -> Self {
        AssetError::InternalSystemError(
            format!("Database Error: {}", db_error)
        )
    }
}

impl ResponseError for AssetError{
    // TODO: Could this be replaced with a macro, maybe with a call per enum value?
    fn status_code(&self) -> actix_web::http::StatusCode {
        match self {
            Self::AssetNotFound(_) => StatusCode::NOT_FOUND,
            Self::InternalSystemError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Self::UnauthorizedError => StatusCode::UNAUTHORIZED,
        }
    }
    // TODO: add this via macro
    fn error_response(&self) -> HttpResponse<actix_web::body::BoxBody> {
        let mut resp = HttpResponse::new(self.status_code());
        let mut buf = BytesMut::new();
        let _ = write!(&mut buf, r#"{{"error": "{}"}}"#, self);
        resp.headers_mut().insert(
            header::CONTENT_TYPE,
            HeaderValue::from_static("application/json; charset=utf-8")
        );
        resp.set_body(buf).map_into_boxed_body()
    }
}