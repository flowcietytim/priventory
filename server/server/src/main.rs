pub mod config;
mod api;

use actix_web::{App, HttpServer, web::{Data, scope}};
use actix_web_httpauth::middleware::HttpAuthentication;
use api::{configure_public_apis, configure_secure_apis, auth_middleware};
use crate::config::Configuration;
use commons::storage::Database;


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let config = Data::new(Configuration::from_env().expect("Error loading configuration!"));
    let storage = Data::new(Database::new(&config.db_url).await.expect("Connection with database failed"));
    // TODO: Understand why the variables must be assigned before the move
    let bind_address = config.webservice_bind_address.clone();
    let port = config.webservice_port;
    storage.migrate().await.expect("Error on database migration.");
    HttpServer::new(move || {
        App::new()
            .app_data(storage.clone())
            .app_data(config.clone())
            .service(
                scope("/public")
                .configure(configure_public_apis)
            )
            .service(
                scope("/api")
                .wrap(HttpAuthentication::bearer(auth_middleware))
                .configure(configure_secure_apis)
            )
    })
    .bind((bind_address, port))?
    .run()
    .await
}
