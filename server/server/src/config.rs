use config::{ConfigError, File, Config, Environment};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Configuration {
    pub db_url: String,
    pub webservice_bind_address: String,
    pub webservice_port: u16,
    pub jwt_secret: String, // secret used to create jwt en-/decoding
}

// this allows easy printing
impl std::fmt::Debug for Configuration {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Configuration")
            .field("db_url", &self.db_url)
            .field("webservice_bind_address", &self.webservice_bind_address)
            .field("webservice_port", &self.webservice_port)
            .finish()
    }
}

impl Configuration {
    pub fn from_env() -> Result<Self, ConfigError> {
        // compile default file into code to avoid having to deploy it
        let builder = Config::builder().add_source(
            File::from_str(
                include_str!("../config/default.toml"), config::FileFormat::Toml,
            ));

        // overwrite any default value with env values
        builder.add_source(Environment::default()).build()?.try_deserialize()
    }
}