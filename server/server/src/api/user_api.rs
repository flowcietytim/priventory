use std::collections::HashMap;

use actix_web::{web::{self, Data, Json, ServiceConfig, Path}, post, Responder, HttpResponse, error::{self}, get};
use actix_web_grants::proc_macro::has_roles;
use chrono::Duration;
use commons::storage::Database;

use user::{model::user::{self as model, User}, LoginData, repository::UserRepository, auth::LoginToken};

use crate::config::Configuration;

#[post("")]
#[has_roles("1")]
async fn post_user(
    user_json: Json<User>,
    storage: Data<Database>
) -> impl Responder {
    let user = user_json.into_inner();
    let result = model::create_user(
        user, UserRepository::new(storage.into_inner())
    )
    .await;
    result
        .map_err(error::ErrorInternalServerError)
        .map(|ok| HttpResponse::Created().json(ok))
}

#[get("")]
async fn get_all_users(storage: Data<Database>) -> impl Responder {
    model::get_all_users(UserRepository::new(storage.into_inner()))
    .await
    .map(|users| HttpResponse::Ok().json(users))
}

#[get("/{id}")]
async fn get_user(id: Path<i32>, storage: Data<Database>) -> impl Responder {
    model::get_user_by_id(
        id.into_inner(),
        UserRepository::new(storage.into_inner())
    )
    .await
    .map(|user| HttpResponse::Ok().json(user))
}

#[post("/login")]
async fn login(
    params: web::Json<LoginData>,
    config: Data<Configuration>,
    storage: Data<Database>
) -> impl Responder {
    model::check_user_login(
        &params,
        UserRepository::new(storage.into_inner())
    )
    .await
    .and_then(|user| {
            let token = LoginToken::from_user(Duration::minutes(20i64), &user)
                .encode_to_jwt(&config.jwt_secret)?;
            let mut result = HashMap::new();
            result.insert("token", token);
            Ok(result)
    })
    .map_err(error::ErrorInternalServerError)
    .map(|result| HttpResponse::Ok().json(result))
    
}

pub fn configure_routes(cfg: &mut ServiceConfig) {
    cfg.service(post_user);
    cfg.service(get_user);
    cfg.service(get_all_users);
}
pub fn configure_public_routes(cfg: &mut ServiceConfig) {
    cfg.service(login);
}