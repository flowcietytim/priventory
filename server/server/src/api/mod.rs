mod user_api;
mod role_api;
mod asset_api;

use actix_web::dev::ServiceRequest;
use actix_web::error::{ErrorInternalServerError, ErrorUnauthorized};
use actix_web::web::{ServiceConfig, Data};
use actix_web::{get, Responder, HttpResponse, HttpMessage};
use actix_web_grants::permissions::AttachPermissions;
use actix_web_httpauth::extractors::bearer::BearerAuth;
use git_version::git_version;
use user::auth::LoginToken;

use crate::config::Configuration;

/// Return the version of the application.
#[get("/version")]
async fn version() -> impl Responder {
    HttpResponse::Ok().body(git_version!())
}

/// Simple method to use as a healthcheck.
#[get("/healthcheck")]
async fn healthcheck() -> impl Responder {
    HttpResponse::Ok()
}

pub(crate) async fn auth_middleware(req: ServiceRequest, encoded_token: BearerAuth) -> Result<ServiceRequest, actix_web::Error> {
    let config = req.app_data::<Data<Configuration>>().ok_or_else(|| ErrorInternalServerError("Could not access server config"))?;
    let token = LoginToken::decode_jwt(
        &config.jwt_secret,
        encoded_token.token()
    )
    .map_err(|e| ErrorUnauthorized(e.to_string()))?;
    req.attach(token.get_roles().to_owned());
    req.extensions_mut().insert(token);
    Ok(req)
}

pub fn configure_public_apis(cfg: &mut ServiceConfig) {
    cfg
        .service(healthcheck)
        .service(version)
        .service(
            actix_web::web::scope("/user")
                .configure(user_api::configure_public_routes)
        )
        .service(
            actix_web::web::scope("/role")
            .configure(role_api::configure_public_routes)
        )
        .service(
            actix_web::web::scope("/asset")
            .configure(asset_api::configure_public_routes)
        );
}

pub fn configure_secure_apis(cfg: &mut ServiceConfig) {
    cfg.service(
        actix_web::web::scope("/user")
        .configure(user_api::configure_routes)
    ).service(
        actix_web::web::scope("/role")
        .configure(role_api::configure_routes)
    ).service(
        actix_web::web::scope("/asset")
        .configure(asset_api::configure_routes)
    );
}