use actix_web::{web::{self, Data, ServiceConfig, Path}, post, Responder, HttpResponse, get};
use actix_web_grants::proc_macro::has_roles;
use commons::storage::Database;
use user::{model::role::{self as model, Role}, repository::UserRepository};

#[get("/")]
async fn get_all_roles(storage: Data<Database>) -> impl Responder {
    model::get_all_roles(UserRepository::new(storage.into_inner()))
    .await
    .map(|roles| HttpResponse::Ok().json(roles))
}

#[get("/{id}")]
async fn get_role(id: Path<i32>, storage: Data<Database>) -> impl Responder {
    model::get_role_by_id(
        id.into_inner(),
        UserRepository::new(storage.into_inner())
    )
    .await
    .map(|role| HttpResponse::Ok().json(role))
}

#[post("/")]
#[has_roles("1")]
async fn post_role(
    role_json: web::Json<Role>,
    storage: Data<Database>
) -> impl Responder {
    model::create_role(
        role_json.into_inner(),
        UserRepository::new(storage.into_inner())
    )
    .await
    .map(|result| HttpResponse::Ok().json(result))
    
}

pub fn configure_routes(cfg: &mut ServiceConfig) {
    cfg.service(post_role);
    cfg.service(get_role);
    cfg.service(get_all_roles);
}
pub fn configure_public_routes(_cfg: &mut ServiceConfig) {
}