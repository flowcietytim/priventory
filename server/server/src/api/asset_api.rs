use actix_web::{
    get, post,
    web::{Data, Json, Path, ServiceConfig},
    HttpMessage, HttpRequest, HttpResponse, Responder,
};
use actix_web_grants::proc_macro::has_any_role;
use asset::{
    model::asset::{self as model, Asset},
    repository::AssetRespository,
    AssetError,
};
use commons::storage::Database;
use user::auth::LoginToken;

#[post("")]
#[has_any_role("1", "2")]
async fn post_asset(asset_json: Json<Asset>, storage: Data<Database>) -> impl Responder {
    let asset = asset_json.into_inner();
    model::create_asset(asset, AssetRespository::new(storage.into_inner()))
        .await
        .map(|ok| HttpResponse::Created().json(ok))
}

#[get("")]
async fn get_all_assets(req: HttpRequest, storage: Data<Database>) -> impl Responder {
    let user_id = req
        .extensions()
        .get::<LoginToken>()
        .map(LoginToken::get_user)
        .ok_or(AssetError::UnauthorizedError)?;
    model::get_assets_by_user(user_id, AssetRespository::new(storage.into_inner()))
        .await
        .map(|assets| HttpResponse::Ok().json(assets))
}

#[get("/owned")]
async fn get_owned_assets(req: HttpRequest, storage: Data<Database>) -> impl Responder {
    let user_id = req
        .extensions()
        .get::<LoginToken>()
        .map(LoginToken::get_user)
        .ok_or(AssetError::UnauthorizedError)?;
    model::get_assets_by_owner(user_id, AssetRespository::new(storage.into_inner()))
        .await
        .map(|assets| HttpResponse::Ok().json(assets))
}

#[post("/{asset_id}/use")]
async fn use_asset(
    asset_id: Path<i32>,
    req: HttpRequest,
    storage: Data<Database>,
) -> impl Responder {
    let user_id = req
        .extensions()
        .get::<LoginToken>()
        .map(LoginToken::get_user)
        .ok_or(AssetError::UnauthorizedError)?;
    model::use_asset(
        user_id,
        asset_id.into_inner(),
        AssetRespository::new(storage.into_inner()),
    )
    .await
    .map(|_| HttpResponse::Ok().json(()))
}

pub fn configure_routes(cfg: &mut ServiceConfig) {
    cfg.service(post_asset);
    cfg.service(get_all_assets);
    cfg.service(get_owned_assets);
}
pub fn configure_public_routes(_cfg: &mut ServiceConfig) {}
