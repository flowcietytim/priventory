use sqlx::{pool::PoolConnection, Pool, Postgres, Transaction};
use anyhow::Result;

pub struct Database {
    pool: Pool<Postgres>,
}

impl Database {
    /// Creates and returns a (wrapped) connection pool to the db
    /// 
    /// # Errors
    /// Fails if it could not connect
    pub async fn new(url: &str) -> Result<Self> {
        Ok(Self {
            pool: Pool::connect(url).await?,
        })
    }

    /// Executes migrations defined in .../migrations
    /// 
    /// # Errors
    /// Escalates any DB-site errors
    pub async fn migrate(&self) -> anyhow::Result<()> {
        sqlx::migrate!("../migrations").run(&self.pool).await?;
        Ok(())
    }

    /// Returns a DBConnection from the pool
    pub async fn get_connection(&self) -> sqlx::Result<PoolConnection<Postgres>> {
        self.pool.acquire().await
    }

    /// Creates a new transaction
    /// Should the transaction not be committed, every contained statement is rolled back.
    pub async fn begin_transaction(&self) -> sqlx::Result<Transaction<Postgres>> {
        self.pool.begin().await
    }
}