-- password is a PHC string, containin hash result and salt
CREATE TABLE users (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(100) NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT uk_users_email UNIQUE(email)
);
CREATE TABLE permissions (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    title VARCHAR(100) NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    PRIMARY KEY(id),
    CONSTRAINT uk_permissions_title UNIQUE(title)
);
CREATE TABLE categories (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    parent INTEGER NOT NULL,
    title VARCHAR(100) NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    -- picture as b64
    icon TEXT,
    -- hex color code 3 triplets
    color VARCHAR(6),
    PRIMARY KEY(id),
    CONSTRAINT fk_categories_parent FOREIGN KEY(parent) REFERENCES categories(id),
    CONSTRAINT uk_categories_title UNIQUE(title)
);
CREATE TABLE assets (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(100) NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    owner INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_assets_owner FOREIGN KEY(owner) REFERENCES users(id),
    CONSTRAINT fk_assets_category FOREIGN KEY(category_id) REFERENCES categories(id)
);
CREATE TABLE roles (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
    title VARCHAR(100) NOT NULL,
    description TEXT NOT NULL DEFAULT '',
    PRIMARY KEY(id),
    CONSTRAINT uk_roles_title UNIQUE(title)
);
CREATE TABLE user_roles (
    user_id INTEGER,
    role_id INTEGER,
    CONSTRAINT fk_user_roles_user FOREIGN KEY(user_id) REFERENCES users(id),
    CONSTRAINT fk_user_roles_role FOREIGN KEY(role_id) REFERENCES roles(id)
);
CREATE TABLE grants (
    user_id INTEGER NOT NULL,
    asset_id INTEGER NOT NULL,
    permission_id INTEGER NOT NULL,
    -- technically not necessary, but helpful for compatibility and clarity
    PRIMARY KEY(user_id, asset_id, permission_id),
    CONSTRAINT fk_grants_user FOREIGN KEY(user_id) REFERENCES users(id),
    CONSTRAINT fk_grants_asset FOREIGN KEY(asset_id) REFERENCES assets(id),
    CONSTRAINT fk_grants_permission FOREIGN KEY(permission_id) REFERENCES permissions(id)
);