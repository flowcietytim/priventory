CREATE TABLE usages (
    user_id INTEGER NOT NULL,
    asset_id INTEGER NOT NULL,
    creation_date TIMESTAMPTZ NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC'),
    -- technically not necessary, but helpful for compatibility and clarity
    PRIMARY KEY(user_id, asset_id),
    CONSTRAINT fk_usages_user FOREIGN KEY(user_id) REFERENCES users(id),
    CONSTRAINT fk_usages_asset FOREIGN KEY(asset_id) REFERENCES assets(id)
);