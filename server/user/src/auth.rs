use chrono::{DateTime, Utc, Duration};
use jsonwebtoken::{decode, DecodingKey, Validation, encode, Header, Algorithm, EncodingKey};

use crate::{UserError, model::user::User};
/// Struct defining JWT body, using only registered and public claims
/// see https://www.iana.org/assignments/jwt/jwt.xhtml for available public claims
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct LoginToken {
    // expiration time
    #[serde(with = "chrono::serde::ts_seconds")]
    exp: DateTime<Utc>,
    // user id
    sub: i32,
    // roles
    roles: Vec<String>,
}

impl LoginToken {
    pub fn get_roles(&self) -> &[String] {
        &self.roles
    }

    pub fn get_user(&self) -> i32 {
        self.sub
    }

    /// decode jwt - fails on validation error
    pub fn decode_jwt(private_key: &str, encoded_token: &str) -> Result<Self, UserError> {
        // the decode will fail if the token is expired or not signed with given secret
        let decoded_token = decode::<LoginToken>(
            encoded_token,
            &DecodingKey::from_secret(private_key.as_bytes()),
            &Validation::default()
        ).map_err(|e| UserError::InvalidRequestError(format!("{}", e)))?;
        Ok(decoded_token.claims)
    }

    pub fn from_user(lifetime: Duration, user: &User) -> Self {
        LoginToken {
            exp: Utc::now() + lifetime,
            sub: user.id,
            //actix-web-grants expects all roles to be prefixed with "ROLE_" and all permissions with "PERMISSION"
            roles: user.roles.iter().map(|role| format!("ROLE_{}", role)).collect()
        }
        
    }

    pub fn encode_to_jwt(&self, secret: &str) -> Result<String, UserError> {
        encode(
            &Header::new(Algorithm::HS256),
            self,
            &EncodingKey::from_secret(secret.as_bytes())
        )
        .map_err(|e| UserError::InternalSystemError(format!("{}", e)))
    }
}