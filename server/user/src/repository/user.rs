use sqlx::query;
use std::collections::HashMap;

use crate::model::user::User;

use super::UserRepository;

impl UserRepository {
    /// inserts a given user into the database.
    /// Make sure the password is hashed before handing it over to this function!
    pub(crate) async fn insert_user(&self, user: &User) -> Result<i32, sqlx::Error> {
        let mut transaction = self.db.begin_transaction().await?;
        let user_id = query!(
            r#"INSERT INTO USERS(name,email,password) VALUES ($1, $2, $3) ON CONFLICT(email) DO UPDATE SET name = $1 RETURNING id"#,
            user.name,
            user.email,
            user.password)
            .fetch_one(&mut transaction)
            .await?;
        // write all roles the user holds
        for role in &user.roles {
            query!(
                r#"INSERT INTO user_roles(user_id, role_id) VALUES ($1, $2)"#,
                user_id.id,
                role
            )
            .execute(&mut transaction)
            .await?;
        }
        // write all permission the user holds
        for (asset_id, permission) in user
            .permissions
            .iter()
            .flat_map(|(asset_id, permissions)| permissions.iter().map(move |p| (asset_id, p)))
        {
            query!(
                r#"INSERT INTO grants(user_id, asset_id, permission_id) VALUES($1, $2, $3)"#,
                user.id,
                asset_id,
                permission
            )
            .execute(&mut transaction)
            .await?;
        }
        transaction.commit().await?;
        Ok(user_id.id)
    }

    pub(crate) async fn all_users(&self) -> Result<Vec<User>, sqlx::Error> {
        let mut conn = self.db.get_connection().await?;
        let results = query!(
            r#"
            SELECT u.id, u.name, u.email, array_remove(array_agg(ur.role_id), NULL) as roles
            FROM users u 
            LEFT JOIN user_roles ur ON u.id = ur.user_id
            GROUP BY u.id"#
        )
        .fetch_all(&mut conn)
        .await?;
        let mut users = Vec::new();
        for record in results {
            let user = User {
                id: record.id,
                name: record.name.clone(),
                email: record.email.clone(),
                password: "".to_string(),
                roles: if record.roles.is_none() {
                    Vec::new()
                } else {
                    record.roles.unwrap()
                },
                permissions: self.permissions_by_user(record.id).await?,
            };
            users.push(user);
        }
        Ok(users)
    }

    pub(crate) async fn user_by_mail(&self, email: &str) -> Result<Option<User>, sqlx::Error> {
        let mut conn = self.db.get_connection().await?;
        let user_opt = query!(
            r#"
            SELECT u.id, u.name, u.email, array_remove(array_agg(ur.role_id), NULL) as roles
            FROM users u 
            LEFT JOIN user_roles ur ON u.id = ur.user_id WHERE u.email = $1
            GROUP BY u.id"#,
            email
        )
        .fetch_optional(&mut conn)
        .await?
        .map(|result| User {
            id: result.id,
            name: result.name,
            email: result.email,
            password: "".to_string(),
            roles: result.roles.unwrap_or_default(),
            permissions: HashMap::default(),
        });
        if let Some(mut user) = user_opt {
            user.permissions = self.permissions_by_user(user.id).await?;
            Ok(Some(user))
        } else {
            Ok(None)
        }
    }

    pub(crate) async fn user_by_id(&self, id: i32) -> Result<Option<User>, sqlx::Error> {
        let mut conn = self.db.get_connection().await?;
        let user_opt = query!(
            r#"
            SELECT u.id, u.name, u.email, array_remove(array_agg(ur.role_id), NULL) as roles
            FROM users u 
                LEFT JOIN user_roles ur ON u.id = ur.user_id
            WHERE id = $1
            GROUP BY u.id"#,
            id
        )
        .fetch_optional(&mut conn)
        .await?
        .map(|result| User {
            id: result.id,
            name: result.name,
            email: result.email,
            password: "".to_string(),
            roles: if result.roles.is_none() {
                Vec::new()
            } else {
                result.roles.unwrap()
            },
            permissions: HashMap::default(),
        });
        if let Some(mut user) = user_opt {
            user.permissions = self.permissions_by_user(id).await?;
            Ok(Some(user))
        } else {
            Ok(None)
        }
    }

    pub(crate) async fn get_login_data(&self, email: &str) -> Result<Option<String>, sqlx::Error> {
        let mut conn = self.db.get_connection().await?;
        let query_result = query!(r#"SELECT password FROM users WHERE email = $1"#, email)
            .fetch_optional(&mut conn)
            .await?
            .map(|result| result.password);
        Ok(query_result)
    }
}
