use commons::storage::Database;
use std::sync::Arc;

pub mod permission;
pub mod role;
pub mod user;
pub struct UserRepository {
    db: Arc<Database>,
}

impl UserRepository {
    pub fn new(db: Arc<Database>) -> Self {
        Self { db }
    }
}
