use sqlx::{query, query_as};

use crate::model::role::Role;

use super::UserRepository;

impl UserRepository {

    /// inserts a given role into the database.
    pub(crate) async fn insert_role(&self, role: &Role) -> Result<i32, sqlx::Error> {
        let mut transaction = self.db.begin_transaction().await?;
        let role_id = query!(
            r#"INSERT INTO ROLES(title,description) VALUES ($1, $2) ON CONFLICT(title) DO UPDATE SET title = $1 RETURNING id"#,
            role.title,
            role.description)
            .fetch_one(&mut transaction)
            .await?;
        transaction.commit().await?;
        Ok(role_id.id)
    }

    pub(crate) async fn all_roles(&self) -> Result<Vec<Role>, sqlx::Error> {
        let mut conn = self.db.get_connection().await?;
        let results = query_as!(Role, r#"SELECT * FROM roles"#)
            .fetch_all(&mut conn)
            .await?;
        Ok(results)
    }

    pub(crate) async fn role_by_id(&self, id: i32) -> Result<Option<Role>, sqlx::Error> {
        let mut conn = self.db.get_connection().await?;
        let result = query_as!(Role, r#"SELECT * FROM roles WHERE id = $1"#, id)
        .fetch_optional(&mut conn)
        .await?;
        if let Some(role) = result {
            Ok(Some(role))
        } else {
            Ok(None)
        }
    }
}