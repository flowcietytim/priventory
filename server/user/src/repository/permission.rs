use std::collections::HashMap;

use sqlx::{query, query_as};

use crate::model::permission::Permission;

use super::UserRepository;

impl UserRepository {
    pub(crate) async fn insert_permission(
        &self,
        permission: &Permission,
    ) -> Result<i32, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        let perm_id = query!(
            r#"
            INSERT INTO permissions(title, description) VALUES ($1, $2)
            ON CONFLICT(title) DO UPDATE SET title = $1 RETURNING id;
            "#,
            permission.title,
            permission.description
        )
        .fetch_one(&mut connection)
        .await?;
        Ok(perm_id.id)
    }

    pub(crate) async fn all_permissions(&self) -> Result<Vec<Permission>, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        let results = query_as!(Permission, r#"SELECT * FROM permissions"#)
            .fetch_all(&mut connection)
            .await?;
        Ok(results)
    }

    pub(crate) async fn permission_by_id(
        &self,
        id: i32,
    ) -> Result<Option<Permission>, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query_as!(
            Permission,
            r#"SELECT p.id, p.title, p.description
            FROM permissions p LEFT JOIN grants g ON p.id = g.permission_id
            WHERE p.id = $1"#,
            id
        )
        .fetch_optional(&mut connection)
        .await
    }

    pub(crate) async fn grant_permission(
        &self,
        user_id: i32,
        asset_id: i32,
        permission_id: i32,
    ) -> Result<(), sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query!(
            r#"INSERT INTO grants(user_id, asset_id, permission_id) VALUES ($1, $2, $3)
            ON CONFLICT(user_id, asset_id, permission_id) DO NOTHING"#,
            user_id,
            asset_id,
            permission_id
        )
        .fetch_all(&mut connection)
        .await?;
        Ok(())
    }

    pub(crate) async fn permissions_by_user(
        &self,
        user_id: i32,
    ) -> Result<HashMap<i32, Vec<i32>>, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        let results = query!(
            r#"SELECT asset_id, array_remove(array_agg(permission_id), NULL) as permissions
            FROM grants
            WHERE user_id = $1
            GROUP BY asset_id"#,
            user_id
        )
        .fetch_all(&mut connection)
        .await?;
        let mut map = HashMap::new();
        for result in results {
            if let Some(permissions) = result.permissions {
                map.insert(result.asset_id, permissions);
            }
        }
        Ok(map)
    }

    pub(crate) async fn user_has_permission(
        &self,
        user_id: i32,
        asset_id: i32,
        permission_id: i32,
    ) -> Result<bool, sqlx::Error> {
        let mut connection = self.db.get_connection().await?;
        query!(
            r#"SELECT user_id FROM grants
            WHERE user_id = $1
            AND asset_id = $2
            AND permission_id =$3"#,
            user_id,
            asset_id,
            permission_id
        )
        .fetch_optional(&mut connection)
        .await?
        .map_or(Ok(false), |_| Ok(true))
    }
}
