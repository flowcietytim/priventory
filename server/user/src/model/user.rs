use pbkdf2::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Pbkdf2,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::{repository::UserRepository, LoginData, UserError};

#[derive(Eq, PartialEq, Debug, Clone)]
pub enum UserRoles {
    ADMIN,
    USER,
}

//TODO: Remove password from debug
#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct User {
    #[serde(default)]
    pub id: i32,
    pub name: String,
    pub email: String,
    #[serde(skip_serializing)]
    pub password: String,
    // role IDs
    pub roles: Vec<i32>,
    // key of asset id with an associated vec of permission IDs
    pub permissions: HashMap<i32, Vec<i32>>,
}

pub async fn create_user(mut user: User, repo: UserRepository) -> Result<User, UserError> {
    let mut unknown_roles = Vec::new();
    for role in &user.roles {
        if repo.role_by_id(*role).await.is_err() {
            unknown_roles.push(role.to_string());
        }
    }
    if !unknown_roles.is_empty() {
        return Err(UserError::RoleNotFound(format!("{:?}", unknown_roles)));
    }
    user.password = hash_password(&user.password)?;
    user.id = repo
        .insert_user(&user)
        .await
        .map_err(UserError::from_sqlx)?;
    Ok(user)
}

pub async fn get_all_users(repo: UserRepository) -> Result<Vec<User>, UserError> {
    let users = repo.all_users().await.map_err(UserError::from_sqlx)?;
    Ok(users)
}

pub async fn get_user_by_email(email: &str, repo: UserRepository) -> Result<User, UserError> {
    let user_opt = repo
        .user_by_mail(email)
        .await
        .map_err(UserError::from_sqlx)?;
    match user_opt {
        Some(user) => Ok(user),
        None => Err(UserError::UserNotFound(email.to_owned())),
    }
}
pub async fn get_user_by_id(id: i32, repo: UserRepository) -> Result<User, UserError> {
    let user_opt = repo.user_by_id(id).await.map_err(UserError::from_sqlx)?;
    match user_opt {
        Some(user) => Ok(user),
        None => Err(UserError::UserNotFound(id.to_string())),
    }
}

pub async fn check_user_login(
    login_data: &LoginData,
    repo: UserRepository,
) -> Result<User, UserError> {
    let stored_pw = repo
        .get_login_data(&login_data.email)
        .await
        .map_err(UserError::from_sqlx)?
        .ok_or(UserError::InvalidCredentialError)?;
    let hashed_pw =
        PasswordHash::new(&stored_pw).map_err(|e| UserError::HashError(format!("{}", e)))?;
    Pbkdf2
        .verify_password(login_data.password.as_bytes(), &hashed_pw)
        .map_err(|_| UserError::InvalidCredentialError)?;
    let user = get_user_by_email(&login_data.email, repo).await?;
    Ok(user)
}

fn hash_password(raw_password: &str) -> Result<String, UserError> {
    let salt = SaltString::generate(&mut rand_core::OsRng);
    let hashed_pw = Pbkdf2
        .hash_password(raw_password.as_bytes(), salt.as_ref())
        .map_err(|err| UserError::InternalSystemError(format!("Cryptographic Error: {}", err)))?;
    Ok(hashed_pw.to_string())
}
