use serde::{Deserialize, Serialize};

use crate::{repository::UserRepository, UserError};

#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Role {
    #[serde(default)]
    pub id: i32,
    pub title: String,
    pub description: String,
}

pub async fn create_role(mut role: Role, repo: UserRepository) -> Result<Role, UserError> {
    role.id = repo
        .insert_role(&role)
        .await
        .map_err(UserError::from_sqlx)?;
    Ok(role)
}

pub async fn get_all_roles(repo: UserRepository) -> Result<Vec<Role>, UserError> {
    let roles = repo.all_roles().await.map_err(UserError::from_sqlx)?;
    Ok(roles)
}

pub async fn get_role_by_id(id: i32, repo: UserRepository) -> Result<Role, UserError> {
    repo.role_by_id(id)
        .await
        .map_err(UserError::from_sqlx)?
        .ok_or_else(|| UserError::RoleNotFound(id.to_string()))
}
