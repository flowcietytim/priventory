use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{repository::UserRepository, UserError};

#[derive(Eq, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Permission {
    pub id: i32,
    pub title: String,
    pub description: String,
}

pub async fn create_permission(
    mut permission: Permission,
    repo: UserRepository,
) -> Result<Permission, UserError> {
    permission.id = repo
        .insert_permission(&permission)
        .await
        .map_err(UserError::from_sqlx)?;
    Ok(permission)
}

pub async fn get_all_permissions(repo: UserRepository) -> Result<Vec<Permission>, UserError> {
    repo.all_permissions()
        .await
        .map(Ok)
        .map_err(UserError::from_sqlx)?
}

pub async fn get_permission_by_id(id: i32, repo: UserRepository) -> Result<Permission, UserError> {
    repo.permission_by_id(id)
        .await
        .map_err(UserError::from_sqlx)?
        .ok_or_else(|| UserError::PermissionNotFound(id.to_string()))
}

pub async fn get_permissions_by_user(
    user_id: i32,
    repo: UserRepository,
) -> Result<HashMap<i32, Vec<i32>>, UserError> {
    repo.permissions_by_user(user_id)
        .await
        .map_err(UserError::from_sqlx)
}

pub async fn has_permission(
    user_id: i32,
    asset_id: i32,
    permission_id: i32,
    repo: UserRepository,
) -> Result<bool, UserError> {
    repo.user_has_permission(user_id, asset_id, permission_id)
        .await
        .map_err(UserError::from_sqlx)
}

pub async fn grant_permission(
    user_id: i32,
    asset_id: i32,
    permission_id: i32,
    repo: UserRepository,
) -> Result<(), UserError> {
    repo.grant_permission(user_id, asset_id, permission_id)
        .await
        .map_err(UserError::from_sqlx)
}
