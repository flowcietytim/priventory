use serde::Deserialize;
use actix_web::{ResponseError, http::{StatusCode, header::{self, HeaderValue}}, HttpResponse, web::BytesMut};
use std::fmt::Write;


pub mod model;
pub mod repository;
pub mod auth;

#[derive(Deserialize)]
pub struct LoginData {
    email: String,
    password: String
}

/// All errors related to user handling
#[derive(thiserror::Error, Debug)]
pub enum UserError {
    #[error("No matching user found: {0}")]
    UserNotFound(String),
    #[error("{0}")]
    WrappedError(String, StatusCode),
    #[error("Error hashing Data, Details: {0}")]
    HashError(String),
    #[error("No user found matching the provided credentials.")]
    InvalidCredentialError,
    #[error("An unexpected server error occured.")]
    InternalSystemError(String),
    #[error("The request could not be parsed: {0}")]
    InvalidRequestError(String),
    #[error("No matching role found: {0}")]
    RoleNotFound(String),
    #[error("No matching permission found: {0}")]
    PermissionNotFound(String),
}

impl UserError {
    pub fn from_sqlx(db_error: sqlx::Error) -> Self {
        UserError::InternalSystemError(
            format!("Database Error: {}", db_error)
        )
    }
}

impl ResponseError for UserError{
    // TODO: Could this be replaced with a macro, maybe with a call per enum value?
    fn status_code(&self) -> actix_web::http::StatusCode {
        match self {
            Self::UserNotFound(_) | Self::RoleNotFound(_) | Self::PermissionNotFound(_) => StatusCode::NOT_FOUND,
            Self::WrappedError(_, status) => *status,
            Self::HashError(_) | Self::InternalSystemError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Self::InvalidCredentialError | Self::InvalidRequestError(_) => StatusCode::BAD_REQUEST
        }
    }
    // TODO: add this via macro
    fn error_response(&self) -> HttpResponse<actix_web::body::BoxBody> {
        let mut resp = HttpResponse::new(self.status_code());
        let mut buf = BytesMut::new();
        let _ = write!(&mut buf, r#"{{"error": "{}"}}"#, self);
        resp.headers_mut().insert(
            header::CONTENT_TYPE,
            HeaderValue::from_static("application/json; charset=utf-8")
        );
        resp.set_body(buf).map_into_boxed_body()
    }
}

#[cfg(test)]
mod tests {}